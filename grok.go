//
// Copyright (c) 2016-2017 Konstanin Ivanov <kostyarin.ivanov@gmail.com>.
// All rights reserved. This program is free software. It comes without
// any warranty, to the extent permitted by applicable law. You can
// redistribute it and/or modify it under the terms of the Do What
// The Fuck You Want To Public License, Version 2, as published by
// Sam Hocevar. See LICENSE file for more details or see below.
//

//
//        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                    Version 2, December 2004
//
// Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//  0. You just DO WHAT THE FUCK YOU WANT TO.
//

// Package grokky is a pure Golang Grok-like patterns library. This can
// help you to parse log files and other. This is based on RE2 regexp
// that much more faster then Oniguruma. The library disigned for creating
// many patterns and using it many times. The behavior and capabilities
// are slightly different from the original library. The golas of the
// library are: (1) simplicity, (2) performance, (3) ease of use.
package grokky

// http://play.golang.org/p/vb18r_OZkK

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"
)

var patternRegexp = regexp.MustCompile(`\%\{(\w+)(\:(\w+))?}`)

var (
	// ErrEmptyName arises when pattern name is an empty string
	ErrEmptyName = errors.New("an empty name")
	// ErrEmptyExpression arises when expression is an empty string
	ErrEmptyExpression = errors.New("an empty expression")
	// ErrAlreadyExist arises when pattern with given name alrady exists
	ErrAlreadyExist = errors.New("the pattern already exist")
	// ErrNotExist arises when pattern with given name doesn't exists
	ErrNotExist = errors.New("pattern doesn't exist")
)

// helpers

func split(s string) (name, sem string) {
	ss := patternRegexp.FindStringSubmatch(s)
	if len(ss) >= 2 {
		name = ss[1]
	}
	if len(ss) >= 4 {
		sem = ss[3]
	}
	return
}

func wrap(s string) string { return "(" + s + ")" }

// host

// Host is a patterns collection. Feel free to
// delete the Host after all patterns (that you need)
// are created. Think of it as a kind of factory.
type Host map[string]string

// New returns new empty host
func New() Host {
    h := make(Host)
	h["USERNAME"] = `[a-zA-Z0-9._-]+`
	h["USER"] = `%{USERNAME}`
	h["EMAILLOCALPART"] = `[a-zA-Z][a-zA-Z0-9_.+-=:]+`
	h["HOSTNAME"] = `\b[0-9A-Za-z][0-9A-Za-z-]{0,62}(?:\.[0-9A-Za-z][0-9A-Za-z-]{0,62})*(\.?|\b)`
	h["EMAILADDRESS"] = `%{EMAILLOCALPART}@%{HOSTNAME}`
	h["HTTPDUSER"] = `%{EMAILADDRESS}|%{USER}`
	h["INT"] = `[+-]?(?:[0-9]+)`
	h["BASE10NUM"] = `[+-]?(?:(?:[0-9]+(?:\.[0-9]+)?)|(?:\.[0-9]+))`
	h["NUMBER"] = `%{BASE10NUM}`
	h["BASE16NUM"] = `[+-]?(?:0x)?(?:[0-9A-Fa-f]+)`
	h["BASE16FLOAT"] = `\b[+-]?(?:0x)?(?:(?:[0-9A-Fa-f]+(?:\.[0-9A-Fa-f]*)?)|(?:\.[0-9A-Fa-f]+))\b`
	h["POSINT"] = `\b[1-9][0-9]*\b`
	h["NONNEGINT"] = `\b[0-9]+\b`
	h["WORD"] = `\b\w+\b`
	h["NOTSPACE"] = `\S+`
	h["SPACE"] = `\s*`
	h["DATA"] = `.*?`
	h["GREEDYDATA"] = `.*`
	h["QUOTEDSTRING"] = `("(\\.|[^\\"]+)+")|""|('(\\.|[^\\']+)+')|''|`+
		"(`(\\\\.|[^\\\\`]+)+`)|``"
	h["UUID"] = `[A-Fa-f0-9]{8}-(?:[A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}`
	h["CISCOMAC"] = `(?:[A-Fa-f0-9]{4}\.){2}[A-Fa-f0-9]{4}`
	h["WINDOWSMAC"] = `(?:[A-Fa-f0-9]{2}-){5}[A-Fa-f0-9]{2}`
	h["COMMONMAC"] = `(?:[A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}`
	h["MAC"] = `%{CISCOMAC}|%{WINDOWSMAC}|%{COMMONMAC}`
	h["IPV6"] = `((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?`
	h["IPV4"] = `(?:(?:[0-1]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])[.](?:[0-1]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])[.](?:[0-1]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])[.](?:[0-1]?[0-9]{1,2}|2[0-4][0-9]|25[0-5]))`
	h["IP"] = `%{IPV6}|%{IPV4}`
	h["IPORHOST"] = `%{IP}|%{HOSTNAME}`
	h["HOSTPORT"] = `%{IPORHOST}:%{POSINT}`

	// paths
	h["UNIXPATH"] = `(/([\w_%!$@:.,~-]+|\\.)*)+`
	h["TTY"] = `/dev/(pts|tty([pq])?)(\w+)?/?(?:[0-9]+)`
	h["WINPATH"] = `(?:[A-Za-z]+:|\\)(?:\\[^\\?*]*)+`
	h["PATH"] = `%{UNIXPATH}|%{WINPATH}`
	h["URIPROTO"] = `[A-Za-z]+(\+[A-Za-z+]+)?`
	h["URIHOST"] = `%{IPORHOST}(?::%{POSINT:port})?`
	// uripath comes loosely from RFC1738, but mostly from what Firefox
	// doesn't turn into %XX
	h["URIPATH"] = `(?:/[A-Za-z0-9$.+!*'(){},~:;=@#%_\-]*)+`
	h["URIPARAM"] = `\?[A-Za-z0-9$.+!*'|(){},~@#%&/=:;_?\-\[\]<>]*`
	h["URIPATHPARAM"] = `%{URIPATH}(?:%{URIPARAM})?`
	h["URI"] = `%{URIPROTO}://(?:%{USER}(?::[^@]*)?@)?(?:%{URIHOST})?(?:%{URIPATHPARAM})?`
	// Months: January, Feb, 3, 03, 12, December
	h["MONTH"] = `\bJan(?:uary|uar)?|Feb(?:ruary|ruar)?|M(?:a|ä)?r(?:ch|z)?|Apr(?:il)?|Ma(?:y|i)?|Jun(?:e|i)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|O(?:c|k)?t(?:ober)?|Nov(?:ember)?|De(?:c|z)(?:ember)?\b`
	h["MONTHNUM"] = `0?[1-9]|1[0-2]`
	h["MONTHNUM2"] = `0[1-9]|1[0-2]`
	h["MONTHDAY"] = `(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9]`
	// Days: Monday, Tue, Thu, etc...
	h["DAY"] = `Mon(?:day)?|Tue(?:sday)?|Wed(?:nesday)?|Thu(?:rsday)?|Fri(?:day)?|Sat(?:urday)?|Sun(?:day)?`
	// Years?
	h["YEAR"] = `(?:\d\d){1,2}`
	h["HOUR"] = `2[0123]|[01]?[0-9]`
	h["MINUTE"] = `[0-5][0-9]`
	// '60' is a leap second in most time standards and thus is valid.
	h["SECOND"] = `(?:[0-5]?[0-9]|60)(?:[:.,][0-9]+)?`
	h["TIME"] = `%{HOUR}:%{MINUTE}:%{SECOND}`
	// datestamp is YYYY/MM/DD-HH:MM:SS.UUUU (or something like it)
	h["DATE_US"] = `%{MONTHNUM}[/-]%{MONTHDAY}[/-]%{YEAR}`
	h["DATE_EU"] = `%{MONTHDAY}[./-]%{MONTHNUM}[./-]%{YEAR}`
	// I really don't know how it's called
	h["DATE_X"] = `%{YEAR}/%{MONTHNUM2}/%{MONTHDAY}`
	h["ISO8601_TIMEZONE"] = `Z|[+-]%{HOUR}(?::?%{MINUTE})`
	h["ISO8601_SECOND"] = `%{SECOND}|60`
	h["TIMESTAMP_ISO8601"] = `%{YEAR}-%{MONTHNUM}-%{MONTHDAY}[T ]%{HOUR}:?%{MINUTE}(?::?%{SECOND})?%{ISO8601_TIMEZONE}?`
	h["DATE"] = `%{DATE_US}|%{DATE_EU}|%{DATE_X}`
	h["DATESTAMP"] = `%{DATE}[- ]%{TIME}`
	h["TZ"] = `[A-Z]{3}`
	h["NUMTZ"] = `[+-]\d{4}`
	h["DATESTAMP_RFC822"] = `%{DAY} %{MONTH} %{MONTHDAY} %{YEAR} %{TIME} %{TZ}`
	h["DATESTAMP_RFC2822"] = `%{DAY}, %{MONTHDAY} %{MONTH} %{YEAR} %{TIME} %{ISO8601_TIMEZONE}`
	h["DATESTAMP_OTHER"] = `%{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{TZ} %{YEAR}`
	h["DATESTAMP_EVENTLOG"] = `%{YEAR}%{MONTHNUM2}%{MONTHDAY}%{HOUR}%{MINUTE}%{SECOND}`
	h["HTTPDERROR_DATE"] = `%{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{YEAR}`
	// golang time patterns
	h["ANSIC"] = `%{DAY} %{MONTH} [_123]\d %{TIME} %{YEAR}"`
	h["UNIXDATE"] = `%{DAY} %{MONTH} [_123]\d %{TIME} %{TZ} %{YEAR}`
	h["RUBYDATE"] = `%{DAY} %{MONTH} [0-3]\d %{TIME} %{NUMTZ} %{YEAR}`
	h["RFC822Z"] = `[0-3]\d %{MONTH} %{YEAR} %{TIME} %{NUMTZ}`
	h["RFC850"] = `%{DAY}, [0-3]\d-%{MONTH}-%{YEAR} %{TIME} %{TZ}`
	h["RFC1123"] = `%{DAY}, [0-3]\d %{MONTH} %{YEAR} %{TIME} %{TZ}`
	h["RFC1123Z"] = `%{DAY}, [0-3]\d %{MONTH} %{YEAR} %{TIME} %{NUMTZ}`
	h["RFC3339"] = `%{YEAR}-[01]\d-[0-3]\dT%{TIME}%{ISO8601_TIMEZONE}`
	h["RFC3339NANO"] = `%{YEAR}-[01]\d-[0-3]\dT%{TIME}\.\d{9}%{ISO8601_TIMEZONE}`
	h["KITCHEN"] = `\d{1,2}:\d{2}(AM|PM|am|pm)`
	// Syslog Dates: Month Day HH:MM:SS
	h["SYSLOGTIMESTAMP"] = `%{MONTH} +%{MONTHDAY} %{TIME}`
	h["PROG"] = `[\x21-\x5a\x5c\x5e-\x7e]+`
	h["SYSLOGPROG"] = `%{PROG:program}(?:\[%{POSINT:pid}\])?`
	h["SYSLOGHOST"] = `%{IPORHOST}`
	h["SYSLOGFACILITY"] = `<%{NONNEGINT:facility}.%{NONNEGINT:priority}>`
	h["HTTPDATE"] = `%{MONTHDAY}/%{MONTH}/%{YEAR}:%{TIME} %{INT}`
	// Shortcuts
	h["QS"] = `%{QUOTEDSTRING}`
	// Log Levels
	h["LOGLEVEL"] = `[Aa]lert|ALERT|[Tt]race|TRACE|[Dd]ebug|DEBUG|[Nn]otice|NOTICE|[Ii]nfo|INFO|[Ww]arn?(?:ing)?|WARN?(?:ING)?|[Ee]rr?(?:or)?|ERR?(?:OR)?|[Cc]rit?(?:ical)?|CRIT?(?:ICAL)?|[Ff]atal|FATAL|[Ss]evere|SEVERE|EMERG(?:ENCY)?|[Ee]merg(?:ency)?`
	// Log formats
	h["SYSLOGBASE"] = `%{SYSLOGTIMESTAMP:timestamp} (?:%{SYSLOGFACILITY} )?%{SYSLOGHOST:logsource} %{SYSLOGPROG}:`
	h["COMMONAPACHELOG"] = `%{IPORHOST:clientip} %{HTTPDUSER:ident} %{USER:auth} \[%{HTTPDATE:timestamp}\] "(?:%{WORD:verb} %{NOTSPACE:request}(?: HTTP/%{NUMBER:httpversion})?|%{DATA:rawrequest})" %{NUMBER:response} (?:%{NUMBER:bytes}|-)`
	h["COMBINEDAPACHELOG"] = `%{COMMONAPACHELOG} %{QS:referrer} %{QS:agent}`
	h["HTTPD20_ERRORLOG"] = `\[%{HTTPDERROR_DATE:timestamp}\] \[%{LOGLEVEL:loglevel}\] (?:\[client %{IPORHOST:clientip}\] ){0,1}%{GREEDYDATA:errormsg}`
	h["HTTPD24_ERRORLOG"] = `\[%{HTTPDERROR_DATE:timestamp}\] \[%{WORD:module}:%{LOGLEVEL:loglevel}\] \[pid %{POSINT:pid}:tid %{NUMBER:tid}\]( \(%{POSINT:proxy_errorcode}\)%{DATA:proxy_errormessage}:)?( \[client %{IPORHOST:client}:%{POSINT:clientport}\])? %{DATA:errorcode}: %{GREEDYDATA:message}`
	h["HTTPD_ERRORLOG"] = `%{HTTPD20_ERRORLOG}|%{HTTPD24_ERRORLOG}`

	return h
}

// Add a new pattern to the Host. If pattern with given name
// already exists the ErrAlreadyExists will be retuned.
func (h Host) Add(name, expr string) error {
	if name == "" {
		return ErrEmptyName
	}
	if expr == "" {
		return ErrEmptyExpression
	}
	if _, ok := h[name]; ok {
		return ErrAlreadyExist
	}
	if _, err := h.compileExternal(expr); err != nil {
		return err
	}
	h[name] = expr
	return nil
}

func (h Host) compile(name string) (*Pattern, error) {
	expr, ok := h[name]
	if !ok {
		return nil, ErrNotExist
	}
	return h.compileExternal(expr)
}

func (h Host) compileExternal(expr string) (*Pattern, error) {
	// find subpatterns
	subs := patternRegexp.FindAllString(expr, -1)
	// this semantics set
	ts := make(map[string]struct{})
	// chek: does subpatterns exist into this Host?
	for _, s := range subs {
		name, sem := split(s)
		if _, ok := h[name]; !ok {
			return nil, fmt.Errorf("the '%s' pattern doesn't exist", name)
		}
		ts[sem] = struct{}{}
	}
	// if there are not subpatterns
	if len(subs) == 0 {
		r, err := regexp.Compile(expr)
		if err != nil {
			return nil, err
		}
		p := &Pattern{Regexp: r}
		return p, nil
	}
	// split
	spl := patternRegexp.Split(expr, -1)
	// concat it back
	msi := make(map[string]int)
	order := 1 // semantic order
	var res string
	for i := 0; i < len(spl)-1; i++ {
		// split part
		splPart := spl[i]
		order += capCount(splPart)
		// subs part
		sub := subs[i]
		subName, subSem := split(sub)
		p, err := h.compile(subName)
		if err != nil {
			return nil, err
		}
		sub = p.String()
		subNumSubexp := p.NumSubexp()
		subNumSubexp++
		sub = wrap(sub)
		if subSem != "" {
			msi[subSem] = order
		}
		res += splPart + sub
		// add sub semantics to this semantics
		for k, v := range p.s {
			if _, ok := ts[k]; !ok {
				msi[k] = order + v
			}
		}
		// increse the order
		order += subNumSubexp
	} // last spl
	res += spl[len(spl)-1]
	r, err := regexp.Compile(res)
	if err != nil {
		return nil, err
	}
	p := &Pattern{Regexp: r}
	p.s = msi
	return p, nil
}

// Get pattern by name from the Host
func (h Host) Get(name string) (*Pattern, error) {
	return h.compile(name)
}

// Compile and get pattern without name (and without adding it to this Host)
func (h Host) Compile(expr string) (*Pattern, error) {
	if expr == "" {
		return nil, ErrEmptyExpression
	}
	return h.compileExternal(expr)
}

// Pattern is a pattern.
// Feel free to use the Pattern as regexp.Regexp.
type Pattern struct {
	*regexp.Regexp
	s map[string]int
}

// Parse returns map (name->match) on input. The map can be empty.
func (p *Pattern) Parse(input string) map[string]string {
	ss := p.FindStringSubmatch(input)
	r := make(map[string]string)
	if len(ss) <= 1 {
		return r
	}
	for sem, order := range p.s {
		r[sem] = ss[order]
	}
	return r
}

// Names returns all names that this pattern has
func (p *Pattern) Names() (ss []string) {
	ss = make([]string, 0, len(p.s))
	for k := range p.s {
		ss = append(ss, k)
	}
	return
}

var lineRegexp = regexp.MustCompile(`^(\w+)\s+(.+)$`)

func (h Host) addFromLine(line string) error {
	sub := lineRegexp.FindStringSubmatch(line)
	if len(sub) == 0 { // not match
		return nil
	}
	return h.Add(sub[1], sub[2])
}

// AddFromFile appends all patterns from the file to this Host.
func (h Host) AddFromFile(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if err := h.addFromLine(scanner.Text()); err != nil {
			return err
		}
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	return nil
}

// http://play.golang.org/p/1rPuziYhRL

var (
	nonCapLeftRxp  = regexp.MustCompile(`\(\?[imsU\-]*\:`)
	nonCapFlagsRxp = regexp.MustCompile(`\(?[imsU\-]+\)`)
)

// cap count
func capCount(in string) int {
	leftParens := strings.Count(in, "(")
	nonCapLeft := len(nonCapLeftRxp.FindAllString(in, -1))
	nonCapBoth := len(nonCapFlagsRxp.FindAllString(in, -1))
	escapedLeftParens := strings.Count(in, `\(`)
	return leftParens - nonCapLeft - nonCapBoth - escapedLeftParens
}
